package com.example.featurebranching;

import java.util.LinkedList;

public class Utilizador extends Contactos {

    private LinkedList<Contactos> contactos;

    public Utilizador(String numero, String nome,LinkedList<Contactos> contactos) {
        super(numero, nome);
        this.contactos.addAll(contactos);
    }

    public LinkedList<Contactos> getContactos() {
        return contactos;
    }

    public void addContactos(Contactos contacto) {
        if (contacto == null || this.contactos.contains(contacto)){
            System.out.println("contacto invalido ou ja existe");
           return;
        }
        this.contactos.add(contacto);
    }
}
